class AddPhotoToBlog < ActiveRecord::Migration
  def self.up
    add_attachment :blogs, :photo
  end

  def self.down
    remove_attachment :blogs, :photo
  end
end
