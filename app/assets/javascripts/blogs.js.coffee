# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->

  $("#menu-toggle").click (e) ->
    $("#wrapper").toggleClass("toggled");


  $("#new_blog").on("ajax:success", (e, data, status, xhr) ->
    console.log 'success'
  ).bind "ajax:error", (e, xhr, status, error) ->
    console.log 'error'
    blog_form = e.data
    error_container = $("#error_explanation", blog_form)
    error_container_ul = $("ul", error_container)
    error_container.show()  if error_container.is(":hidden")
    $.each xhr.responseJSON, (index, message) ->
      console.log index
      $("<li>").html(index + " " + message).appendTo error_container_ul