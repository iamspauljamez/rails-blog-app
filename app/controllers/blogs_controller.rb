class BlogsController < ApplicationController
 
 def index
  @blogs = Blog.all
 end

 def create
  @blog  = Blog.new(blog_params)

  respond_to do |format|
    if @blog.save
      format.js {
        render :js => "window.location = '#{blogs_path}'"
      }
    else
      format.json { render json: @blog.errors, status: :unprocessable_entity }
    end
  end
 end

 def new
  @blog = Blog.new
 end

 private

  def blog_params
    params.require(:blog).permit(:title, :content, :photo)
  end

end
