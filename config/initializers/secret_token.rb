# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
RailBlogApp::Application.config.secret_key_base = 'e2a0e707838b42effe810c788435bc187a98ebf47b0310218a6bf5cbdf00231897c5ed73abe694ebc4b2b1ef063e55ad5602a24778503f559ec90b76d5466394'
